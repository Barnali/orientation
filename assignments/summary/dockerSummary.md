Docker is a container management service. The keywords of Docker are develop, ship and run anywhere. The whole idea of Docker is for developers to easily develop applications, ship them into containers which can then be deployed anywhere.

The initial release of Docker was in March 2013 and since then, it has become the buzzword for modern world development, especially in the face of Agile-based projects.

dockers overview

![overview](/uploads/01bcab676abdd2c9b2505d84fb6b847d/overview.jpeg)


Features of docker

1. Docker has the ability to reduce the size of development by providing a smaller footprint of the operating system via containers.

2. With containers, it becomes easier for teams across different units, such as development, QA and Operations to work seamlessly across applications.

3. You can deploy Docker containers anywhere, on any physical and virtual machines and even on the cloud.

4. Since Docker containers are pretty lightweight, they are very easily scalable.

Components of Docker
Docker has the following components

Docker for Mac − It allows one to run Docker containers on the Mac OS.

Docker for Linux − It allows one to run Docker containers on the Linux OS.

Docker for Windows − It allows one to run Docker containers on the Windows OS.

Docker Engine − It is used for building Docker images and creating Docker containers.


![engine](/uploads/6e2c10ccff619561c48ca2799fd9eb6a/engine.png)


Docker Hub − This is the registry which is used to host various Docker images.

Docker Compose − This is used to define applications using multiple Docker containers.


The official site for Docker is https://www.docker.com/ The site has all information and documentation about the Docker software. It also has the download links for various operating systems.
Docker for Windows
Once the installer has been downloaded, double-click it to start the installer and then follow the steps given below.

Step 1 − Click on the Agreement terms and then the Install button to proceed ahead with the installation.

![docker_setup](/uploads/2c5c828036090a0d9e1aa3bf14837d6a/docker_setup.jpg)

Step 2 − Once complete, click the Finish button to complete the installation.

![finish](/uploads/660eecc70bc45c57f7b01e05521caab5/finish.jpg)

Uninstall the older version of docker if is already installed.

$ sudo apt-get remove docker docker-engine docker.io containerd runc

Installing the new version of docker is preferrable.

     $ sudo apt-get update
     $ sudo apt-get install \
         apt-transport-https \
         ca-certificates \
         curl \
         gnupg-agent \
         software-properties-common
     $ curl -fsSL https://download.docker.com/linux   /ubuntu/gpg | sudo apt-key add -
     $ sudo apt-key fingerprint 0EBFCD88
     $ sudo add-apt-repository \
        "deb [arch=amd64] https://download.docker.com  /linux/ubuntu \
        $(lsb_release -cs) \
        stable nightly test"
     $ sudo apt-get update
     $ sudo apt-get install docker-ce docker-ce-cli  containerd.io

     // Check if docker is successfully installed  in your system
     $ sudo docker run hello-world


     

     
     
     

## docker command
* docker run – Runs a command in a new container.
* docker start – Starts one or more stopped containers
* docker stop – Stops one or more running containers
* docker build – Builds an image form a Docker file
* docker pull – Pulls an image or a repository from a registry
* docker push – Pushes an image or a repository to a registry
* docker export – Exports a container’s filesystem as a tar archive
* docker exec – Runs a command in a run-time container
* docker search – Searches the Docker Hub for images
* docker attach – Attaches to a running container
* docker commit – Creates a new image from a container’s changes

## Docker Vs Virtual machines

![dckr](/uploads/30344e89df89528fb1d7cc8fabfb234d/dckr.png)

![dock](/uploads/e194b481329d5e7f7b3f685d7d6147fa/dock.png)



