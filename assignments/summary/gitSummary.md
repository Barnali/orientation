<h3>What is Git?</h3>
So, what is Git in a nutshell? This is an important section to absorb, because if you understand what Git is and the fundamentals of how it works, then using Git effectively will probably be much easier for you. 

1. keeps track of code history
2. takes "snapshots" of your files
3. you decide when to take a snapshot by makng a "commit"
4. you can stage files before commitng

![vcs](/uploads/28a92334f2ba653d6b2d11593bb70059/vcs.png)


### Git Generally Only Adds Data
When you do actions in Git, nearly all of them only add data to the Git database. It is hard to get the system to do anything that is not undoable or to make it erase data in any way. As with any VCS, you can lose or mess up changes you haven’t committed yet, but after you commit a snapshot into Git, it is very difficult to lose, especially if you regularly push your database to another repository.
This makes using Git a joy because we know we can experiment without the danger of severely screwing things up. For a more in-depth look at how Git stores its data and how you can recover data that seems lost, see Undoing Things.

### The three states

Pay attention now — here is the main thing to remember about Git if you want the rest of your learning process to go smoothly. Git has three main states that your files can reside in: modified, staged, and committed:
1.  Modified means that you have changed the file but have not committed it to your database yet.

2. Staged means that you have marked a modified file in its current version to go into your next commit snapshot.

3. Committed means that the data is safely stored in your local database.
This leads us to the three main sections of a Git project: the working tree, the staging area, and the Git directory.
The working tree is a single checkout of one version of the project. These files are pulled out of the compressed database in the Git directory and placed on disk for you to use or modify.
The staging area is a file, generally contained in your Git directory, that stores information about what will go into your next commit. Its technical name in Git parlance is the “index”, but the phrase “staging area” works just as well.
The Git directory is where Git stores the metadata and object database for your project. This is the most important part of Git, and it is what is copied when you clone a repository from another computer.
### The basic Git workflow goes something like this:

![flow](/uploads/35d44608701df981dd739c5f4151988f/flow.png)

You modify files in your working tree.
# You selectively stage just those changes you want to be part of your next commit, which adds only those changes to the staging area.
# You do a commit, which takes the files as they are in the staging area and stores that snapshot permanently to your Git directory.
# If a particular version of a file is in the Git directory, it’s considered committed. If it has been modified and was added to the staging area, it is staged. And if it was changed since it was checked out but has not been staged, it is modified. In Git Basics, you’ll learn more about these states and how you can either take advantage of them or skip the staged part entirely.

### Common Terminologies in Git:
These are most commonly used terms in git.



* Repository:
This is Often referred to as a 'Repo'. This is essentially where the source code (files and folders) reside that we use Git to track.



* Commit:
Commit is used to Save the changes in our repository or files.



* Branch:
Simply put, a branch in Git is a lightweight movable pointer to one of these commits. The default branch name in Git is master. As we start making commits, we are given a master branch that points to the last commit made.


* Push:
Pushing is essentially syncing the local repository with remote (accessible over the internet) branches.



* Merge:
This is pretty much self-explanatory. In a very basic sense, it is integrating two branches together.



* Clone:
Again cloning is pretty much what it sounds like. It takes the entire remote (accessible over the internet) repository and/or one or more branches and makes it available locally.



* Fork:
Forking allows us to duplicate an existing repo under our username.

![git](/uploads/a902c029562160fa1e37397aff7b1a24/git.jpg)

### Installing Git:
Git probably didn’t come installed on your computer, so we have to get it there. Luckily, installing git is super easy, whether you’re on Linux, Mac, or Windows.
### For Linux, open the terminal and type:
$ sudo apt-get install git 

It only works on Ubuntu. If not, there is a list of all the [Linux package installation commands](https://git-scm.com/download/linux) for whichever other distro you’re on.

For windows users, Git can be installed by [downloading installer](https://git-scm.com/download/win) and run it.

Similarly for Mac users, [download the installer](https://git-scm.com/download/mac) and run it.

For checking installation is succesfull or not,

* Open git terminal.
* Enter:

```shell
    $ git --version
```

It should give you installed git version.

* Configure your Git username and email using the following commands, replacing user-name and user-emailID with your own. These details will be associated with any commits that you create:

```shell
$ git config --global <user-name>
$ git config --global <user-emailID>
```






* Cloning the repository

$ git clone <link-to-repository>

* Creating a branch.

$ git checkout master
$ git checkout -b <branch-name>

* Modifying files and adding them to the staging area.

$ git add . # '.' marks all the untracked files.

* Committing the staged files to the local repository

$ git commit -sv 

* Uploading the local repository to our remote repository.

$ git push origin <branch-name>
